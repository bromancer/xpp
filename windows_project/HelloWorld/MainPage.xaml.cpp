﻿//
// MainPage.xaml.cpp
// Implementation of the MainPage class.
//

#include "pch.h"
#include "MainPage.xaml.h"
#include "HttpRequestorFactoryImpl.h"
#include "http_stuff_impl.hpp"
#include <memory>

using namespace HelloWorld;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Core;
using namespace concurrency;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

MainPage::MainPage()
{
	InitializeComponent();

	std::shared_ptr<xpp::HttpRequestorFactory> http_requestor_factory = std::make_shared<xpp::HttpRequestorFactoryImpl>();
	this->http_stuff = xpp::HttpStuff::create( http_requestor_factory );
}


void HelloWorld::MainPage::button_Click( Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e )
{
	auto task = create_task( [this] () { 
		// Blocking operation, must not run on UI/Main Thread.
		return this->http_stuff->doSomething(); 
	} ).then( [this] ( std::string s ) {
		std::wstring ws( s.begin(), s.end() );

		this->Dispatcher->RunAsync(CoreDispatcherPriority::Normal, ref new DispatchedHandler([this, ws] () {
			// Update UI on UI/Main Thread.
			this->textBlock->Text = ref new String( ws.c_str() );
		}) );
	} );
}
