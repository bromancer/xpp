#pragma once

#include "http_requestor.hpp"

namespace xpp
{
	class HttpRequestorImpl : public xpp::HttpRequestor
	{
	public:
		HttpRequestorImpl();
		std::string getURL( const std::string & url );
	};

}
