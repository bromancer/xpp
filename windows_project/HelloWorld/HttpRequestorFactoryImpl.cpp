#include "pch.h"

#include "HttpRequestorFactoryImpl.h"
#include "HttpRequestorImpl.h"

namespace xpp
{

	HttpRequestorFactoryImpl::HttpRequestorFactoryImpl()
	{
	}


	HttpRequestorFactoryImpl::~HttpRequestorFactoryImpl()
	{
	}

	std::shared_ptr<HttpRequestor> HttpRequestorFactoryImpl::newInstance()
	{
		return std::make_shared<HttpRequestorImpl>();
	}
}
