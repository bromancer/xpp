#pragma once

#include "http_requestor_factory.hpp"

namespace xpp
{

	class HttpRequestorFactoryImpl : public xpp::HttpRequestorFactory
	{
	public:
		HttpRequestorFactoryImpl();
		~HttpRequestorFactoryImpl();

		std::shared_ptr<HttpRequestor> newInstance();
	};

}
