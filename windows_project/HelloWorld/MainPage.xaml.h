﻿//
// MainPage.xaml.h
// Declaration of the MainPage class.
//

#pragma once

#include "MainPage.g.h"
#include "http_stuff.hpp"

namespace HelloWorld
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public ref class MainPage sealed
	{
	public:
		MainPage();

	private:
		void textBlock_SelectionChanged( Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e );
		void button_Click( Platform::Object^ sender, Windows::UI::Xaml::RoutedEventArgs^ e );
		std::shared_ptr<xpp::HttpStuff> http_stuff;
	};
}
