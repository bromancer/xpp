#include "pch.h"
#include "HttpRequestorImpl.h"
#include <ppltasks.h>

using namespace Windows::Web::Http;
using namespace Windows::Foundation;
using namespace Platform;
using namespace concurrency;

namespace xpp
{
	HttpRequestorImpl::HttpRequestorImpl()
	{
	}

	std::string HttpRequestorImpl::getURL( const std::string & url )
	{
		std::wstring w( url.begin(), url.end() );
		Uri ^ resourceUri = ref new Uri( ref new String(w.c_str()) );

		HttpClient ^ client = ref new HttpClient();


		task<HttpResponseMessage^> task = create_task( client->GetAsync( resourceUri ) );

		task.wait();

		HttpResponseMessage ^ message = task.get();

		auto task_body = create_task( message->Content->ReadAsStringAsync() );

		task_body.wait();

		String ^ s = task_body.get();

		std::string res( s->Begin(), s->End() );

		return res;
	}
}
