#pragma once

#include <string>
#include <memory>
#include "http_stuff.hpp"
#include "http_requestor_factory.hpp"

namespace xpp {
    class HttpStuffImpl : public xpp::HttpStuff {
    private:
        static std::shared_ptr<xpp::HttpRequestorFactory> http_requestor_factory;
    public:
        HttpStuffImpl();
        static std::shared_ptr<xpp::HttpStuff> create(const std::shared_ptr<xpp::HttpRequestorFactory> & factory);
        std::string doSomething();
    };
}