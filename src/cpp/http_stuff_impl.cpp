#include "http_stuff_impl.hpp"
#include "http_requestor.hpp"
#include <string>

namespace xpp {
    std::shared_ptr<xpp::HttpRequestorFactory> HttpStuffImpl::http_requestor_factory;

    std::shared_ptr<xpp::HttpStuff>
    HttpStuff::create(const std::shared_ptr<xpp::HttpRequestorFactory> & factory) {
      return xpp::HttpStuffImpl::create(factory);
    };

    std::shared_ptr<xpp::HttpStuff>
    HttpStuffImpl::create(const std::shared_ptr<xpp::HttpRequestorFactory> & factory) {
        http_requestor_factory = factory;
        return std::make_shared<HttpStuffImpl>();
    };
    
    HttpStuffImpl::HttpStuffImpl() {
        
    };
    
    std::string
    HttpStuffImpl::doSomething() {

        if (http_requestor_factory == nullptr || http_requestor_factory.use_count() == 0) {
            return "Factory has been destroyed";
        }

        std::shared_ptr<xpp::HttpRequestor> requestor = http_requestor_factory->newInstance();

        if (requestor == nullptr || requestor.use_count() == 0) {
            return "Requestor has been destroyed";
        }

        return requestor->getURL("https://httpbin.org/get");
    };
}