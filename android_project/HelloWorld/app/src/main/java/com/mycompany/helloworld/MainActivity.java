package com.mycompany.helloworld;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import net.bromancer.HttpRequestorFactory;
import net.bromancer.HttpStuff;

public class MainActivity extends AppCompatActivity {

    private HttpStuff httpStuffInterface;
    private HttpRequestorFactory httpRequestorFactory;

    static {
        System.loadLibrary("gnustl_shared");
        System.loadLibrary("httpcpp");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        HttpRequestorFactory f = new HttpRequestorFactoryImpl();

        httpStuffInterface = HttpStuff.create(f);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void buttonWasPressed(View view) {
        AsyncTask.execute(new Runnable() {

            /**
             * Starts executing the active part of the class' code. This method is
             * called when a thread is started that has been created with a class which
             * implements {@code Runnable}.
             */
            @Override
            public void run() {
                final String myString = httpStuffInterface.doSomething() + "\n";
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        TextView t = (TextView) findViewById(R.id.helloWorldText);
                        t.setText(myString + t.getText());
                    }
                });
            }
        });
    }
}