package com.mycompany.helloworld;

import net.bromancer.HttpRequestor;
import net.bromancer.HttpRequestorFactory;

/**
 * Created by Michiel on 23-Mar-16.
 */
public class HttpRequestorFactoryImpl extends HttpRequestorFactory {
    @Override
    public HttpRequestor newInstance() {
        return new HttpRequestorImpl();
    }
}
