package com.mycompany.helloworld;

import net.bromancer.HttpRequestor;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Michiel on 23-Mar-16.
 */
public class HttpRequestorImpl extends HttpRequestor {

    public HttpRequestorImpl(){};

    @Override
    public String getURL(String url) {

        URL uri = null;
        HttpURLConnection urlConnection = null;
        StringBuilder responseData = new StringBuilder();

        try {
            uri = new URL(url);
        } catch (MalformedURLException e) {
            return "";
        }

        try {
            urlConnection = (HttpURLConnection) uri.openConnection();
        } catch (IOException e) {
            return "";
        }



        try {
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String line = null;

            while((line = reader.readLine()) != null) {
                responseData.append(line);
            }
        } catch (IOException e) {
            return "";
        }

        return responseData.toString();
    }
}
